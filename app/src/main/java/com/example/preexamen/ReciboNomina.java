package com.example.preexamen;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasTrabajadasNormales;
    private int horasTrabajadasExtra;
    private int puesto;

    private double porcentajeImpuesto = 16.0;
    private static final double PAGO_BASE = 200;

    public ReciboNomina(int numRecibo, String nombre, int horasTrabajadasNormales, int horasTrabajadasExtra, int puesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabajadasNormales = horasTrabajadasNormales;
        this.horasTrabajadasExtra = horasTrabajadasExtra;
        this.puesto = puesto;
    }

    private double calcularPagoPorHora() {
        switch (puesto) {
            case 1:
                return PAGO_BASE * 1.20;
            case 2:
                return PAGO_BASE * 1.50;
            case 3:
                return PAGO_BASE * 2.00;
            default:
                return PAGO_BASE;
        }
    }

    public double calcularSubtotal() {
        double pagoPorHora = calcularPagoPorHora();
        return (horasTrabajadasNormales * pagoPorHora) + (horasTrabajadasExtra * pagoPorHora * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * (porcentajeImpuesto / 100);
    }

    public double calcularTotalPagar() {
        return calcularSubtotal() - calcularImpuesto();
    }
}
