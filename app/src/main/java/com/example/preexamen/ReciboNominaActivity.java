package com.example.preexamen;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    private EditText etNumeroRecibo, etNombre, etHorasNormales, etHorasExtra;
    private RadioGroup radioGroupPuestos;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private TextView lblSubtotalValue, lblImpuestoValue, lblTotalValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        etNumeroRecibo = findViewById(R.id.edtNumRecibo);
        etNombre = findViewById(R.id.edtNombre);
        etHorasNormales = findViewById(R.id.edtHorasNormal);
        etHorasExtra = findViewById(R.id.edtHorasExtras);
        radioGroupPuestos = findViewById(R.id.radioGroupPuestos);

        lblSubtotalValue = findViewById(R.id.lblSubtotalValue);
        lblImpuestoValue = findViewById(R.id.lblImpuestoValue);
        lblTotalValue = findViewById(R.id.lblTotalValue);

        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Obtener los datos del intent
        Intent intent = getIntent();
        String nombre = intent.getStringExtra("nombre");
        int numeroRecibo = intent.getIntExtra("numeroRecibo", -1);

        etNombre.setText(nombre);
        etNumeroRecibo.setText(String.valueOf(numeroRecibo));

        btnCalcular.setOnClickListener(v -> calcularNomina());
        btnLimpiar.setOnClickListener(v -> limpiarCampos());
        btnRegresar.setOnClickListener(v -> finish());
    }

    private void calcularNomina() {
        try {
            int numRecibo = Integer.parseInt(etNumeroRecibo.getText().toString());
            String nombre = etNombre.getText().toString();
            int horasNormales = Integer.parseInt(etHorasNormales.getText().toString());
            int horasExtra = Integer.parseInt(etHorasExtra.getText().toString());
            int puestoId = radioGroupPuestos.getCheckedRadioButtonId();

            RadioButton selectedRadioButton = findViewById(puestoId);
            String puestoString = selectedRadioButton.getText().toString();

            int puesto = 0;
            switch (puestoString) {
                case "Auxiliar":
                    puesto = 1;
                    break;
                case "Albañil":
                    puesto = 2;
                    break;
                case "Ing. Obra":
                    puesto = 3;
                    break;
            }

            ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto);

            double subtotal = recibo.calcularSubtotal();
            double impuesto = recibo.calcularImpuesto();
            double total = recibo.calcularTotalPagar();

            // Actualiza los TextViews con los resultados
            lblSubtotalValue.setText(String.format("%.2f", subtotal));
            lblImpuestoValue.setText(String.format("%.2f", impuesto));
            lblTotalValue.setText(String.format("%.2f", total));

        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor, ingrese todos los campos correctamente.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void limpiarCampos() {
        etNumeroRecibo.setText("");
        etNombre.setText("");
        etHorasNormales.setText("");
        etHorasExtra.setText("");
        radioGroupPuestos.clearCheck();
        lblSubtotalValue.setText("");
        lblImpuestoValue.setText("");
        lblTotalValue.setText("");
    }
}
