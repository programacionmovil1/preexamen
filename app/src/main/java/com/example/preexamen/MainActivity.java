package com.example.preexamen;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private EditText edtNombre;
    private Button btnIrRecibo;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = findViewById(R.id.edtNombre);
        btnIrRecibo = findViewById(R.id.btnIrRecibo);
        imageView = findViewById(R.id.imageView);

        btnIrRecibo.setOnClickListener(v -> {
            String nombre = edtNombre.getText().toString();
            int numeroRecibo = generarNumeroRecibo();

            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtra("nombre", nombre);
            intent.putExtra("numeroRecibo", numeroRecibo);
            startActivity(intent);
        });
    }

    private int generarNumeroRecibo() {
        Random random = new Random();
        return random.nextInt(10000);  // Genera un número de recibo aleatorio entre 0 y 9999
    }
}
